var Buffer = require('buffer/').Buffer

export const detectFace = data => {
    console.log('liat isinya', data)
    const buffer = Buffer.from(data.url,'base64')
    const res = fetch('https://facepermata2.cognitiveservices.azure.com/face/v1.0/detect', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/octet-stream',
            'Ocp-Apim-Subscription-Key': 'b1a703276df1474c8db2437398fe62d9',
        },
        body: buffer,
    })
        .then((response) => response.json())
        .catch((error) => console.error(error))

    return res
}

export const identifyFace = data => {
    // const buffer = Buffer.from(data.url,'base64')
    const res = fetch('https://facepermata2.cognitiveservices.azure.com/face/v1.0/identify', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Ocp-Apim-Subscription-Key': 'b1a703276df1474c8db2437398fe62d9',
        },
        body: JSON.stringify(data),
    })
        .then((response) => response.json())
        .catch((error) => console.error(error))

    return res
}