import { Navigation } from "react-native-navigation";
import Home from '../screens/home'
import detectFace from '../screens/faceDetect/camera'
import fooTabs from '../screens/home/fooTabs'
import inputCode from '../screens/phoneAuth/inputCode'
import checkDevices from '../screens/printerBT/checkDevices'
import printerECS from '../screens/printerBT/printerECS'
import escpos from '../screens/printerBT/escpos'
import tsc from '../screens/printerBT/tsc'


export function registerScreens() {
	Navigation.registerComponent('Home', () => Home);
	Navigation.registerComponent('fooTabs', () => fooTabs);
	Navigation.registerComponent('detectFace', () => detectFace);
	Navigation.registerComponent('inputCode', () => inputCode);
	Navigation.registerComponent('checkDevices', () => checkDevices);
	Navigation.registerComponent('printerECS', () => printerECS);
	Navigation.registerComponent('tsc', () => tsc);
	Navigation.registerComponent('escpos', () => escpos);
}
