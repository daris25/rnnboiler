'use strict';
import React, { PureComponent } from 'react';
import { ToastAndroid, StyleSheet, Text, TouchableOpacity, View, Alert, ImageBackground } from 'react-native';
import { RNCamera } from 'react-native-camera';

import { detectFace, identifyFace } from '../../api/azure'
import { tarikPerson } from '../../api/index';

const PendingView = () => (
  <View
    style={{
      flex: 1,
      backgroundColor: 'lightgreen',
      justifyContent: 'center',
      alignItems: 'center',
    }}
  >
    <Text>Waiting</Text>
  </View>
);

export default class camera extends PureComponent {
  constructor(props) { 
    super(props);
    this.state = { 
      isFaceDetected: false,
      bounds: '',
      origin: '',
      faceArray: '',
      hasil1: 0,
      tampil: false,
      take: true,

      dataPerson : ''
    };
  }

  componentDidMount = async () => {
      // const asu = await tarikAPI()
      // console.log('asi', asu)
  };
  

handleFaceDetected = async faceArray => {

  if (faceArray.faces[0]==null) {
    var bon = {width: 0, height: 0}
    var ori = {x: 0, y: 0}
  } else {
    var bon = faceArray.faces[0].bounds.size
    var ori =faceArray.faces[0].bounds.origin
  }

  this.setState({
        isFaceDetected: true,
        faceArray: faceArray,
        bounds: bon,
        origin: ori
      });

  if (faceArray.faces[0]) {
    
    if (faceArray.faces[0].smilingProbability >= 0.3){
      this.setState({hasil1: 1})
    }
          console.log(this.state.hasil1)
          if (this.state.hasil1 == 1) {
              this.setState({
                take: false
              })

              if (this.state.take) {
                  const options = { 
                    quality: 0.6, base64: true, 
                    fixOrientation: true,
                    // orientation: RNCamera.Constants.Orientation.auto,
                  };
                  const data = await this.camera.takePictureAsync(options);
                    this.setState({tampil: data.uri})

                    ToastAndroid.show('Gambar Telah di ambil ..', ToastAndroid.LONG);
                    console.log('tai',data);
                  
                    const sentDetect ={
                      url    : data.base64,
                    }
                  const detect = await detectFace(sentDetect)
                    console.log('detectnya', detect)

                    if (detect.length !== 0) {
                        const ident ={
                            faceIds    : [detect[0].faceId],
                            personGroupId : 'ip18870'
                        }
                              const identyfi = await identifyFace(ident)
                              console.log('hasil identiry', identyfi)

                              if (identyfi.length !== 0) {
                                    if (identyfi[0].candidates.length !== 0) {
                                          const tarikOrang = await tarikPerson({personID: identyfi[0].candidates[0].personId})
                                            console.log('ini dya', tarikOrang)
                                            this.setState({dataPerson: tarikOrang[0]})
                                    } else {
                                        ToastAndroid.show("Candidate Zero . . ", ToastAndroid.LONG);
                                        console.log('error',identyfi.error)
                                        this.setState({
                                          tampil: false,
                                          take: true,
                                          hasil1: 0
                                        })
                                    }
                              } else {
                                    ToastAndroid.show("Identyfi cannot found ", ToastAndroid.LONG);
                                    console.log('error',identyfi.error)
                                    this.setState({
                                      tampil: false,
                                      take: true,
                                      hasil1: 0
                                    })
                              }
                    } else {
                        ToastAndroid.show("faceId not found . .", ToastAndroid.LONG);
                        console.log('error',detect.error)
                        this.setState({
                          tampil: false,
                          take: true,
                          hasil1: 0
                        })
                    }

                  // if (detect.length !== 0 || detect.length !== 'undefined' ) {
                  //     const ident ={
                  //       faceIds    : [detect[0].faceId],
                  //       personGroupId : 'ip18870'
                  //     }

                  //     const identyfi = await identifyFace(ident)
                  //       console.log('hasil identiry', identyfi)

                  //     if (identyfi.length !== 0 || identyfi.length !== 'undefined') {
                  //         if (identyfi[0].candidates.length !== 0 || identyfi[0].candidates.length !== 'undefined' ) {
                  //             const tarikOrang = await tarikPerson({personID: identyfi[0].candidates[0].personId})
                  //             console.log('ini dya', tarikOrang)
                  //             this.setState({dataPerson: tarikOrang[0]})
                  //         } else {
                  //           ToastAndroid.show("Candidate PersonID not found", ToastAndroid.LONG);
                  //             console.log('error',detect.error)
                  //             this.setState({
                  //               tampil: false,
                  //               take: true,
                  //               hasil1: 0
                  //             })
                  //         }
                  //     } else {
                  //       ToastAndroid.show("Face is not found.", ToastAndroid.LONG);
                  //       console.log('error',detect.error)
                  //       this.setState({
                  //         tampil: false,
                  //         take: true,
                  //         hasil1: 0
                  //       })
                  //     }
                  // } else {
                  //     console.log('error wajah belum terdaftar')
                  //     ToastAndroid.show('Wajah belum terdaftar !!', ToastAndroid.LONG);
                  //     this.setState({
                  //       tampil: false,
                  //       take: true,
                  //       hasil1: 0
                  //     })
                  // }
                    
            
              } else {
                // console.log('gagal1')
              }
          } else {
            // console.log('gagal2')
          }
    } else {
      // console.log('gagal3')
    }
        // console.log('an',faceArray)
        // console.log('bound', this.state.bounds)
        // console.log('origin', this.state.origin)
        // console.log('asi', this.state.isFaceDetected)
  };

   createThreeButtonAlert = () =>
    Alert.alert(
      "FaceID : "+ this.state.dataPerson.faceID,
      "Nama : "+this.state.dataPerson.nama+", "+this.state.dataPerson.alamat ,
      [   
        { text: "OK", 
          onPress: () => this.setState({
            tampil: false,
            take: true,
            hasil1: 0,
            dataPerson: ''
          }) 
        }
      ],
      { cancelable: false }
    );

  render() {
    return (
      <View style={styles.container}>

        {
          this.state.tampil==false ?
              <RNCamera
              ref={(ref)=> this.camera = ref}
              style={styles.preview}
              flashMode={RNCamera.Constants.FlashMode.off}
              type={RNCamera.Constants.Type.front}
              autoFocus={RNCamera.Constants.AutoFocus.on}
              faceDetectionMode={RNCamera.Constants.FaceDetection.Mode.accurate}
              faceDetectionLandmarks={RNCamera.Constants.FaceDetection.Landmarks.all}
              faceDetectionClassifications={RNCamera.Constants.FaceDetection.Classifications.all}
              // onFacesDetected={this.handleFaceDetected}
              onCameraReady={() => this.setState({canDetectFaces: true})}
              onFacesDetected={this.state.canDetectFaces == true ? this.handleFaceDetected : null}

              androidCameraPermissionOptions={{
                title: 'Permission to use camera',
                message: 'We need your permission to use your camera',
                buttonPositive: 'Ok',
                buttonNegative: 'Cancel',
              }}
              androidRecordAudioPermissionOptions={{
                title: 'Permission to use audio recording',
                message: 'We need your permission to use your audio',
                buttonPositive: 'Ok',
                buttonNegative: 'Cancel',
              }}
            >
              {({ camera, status, recordAudioPermissionStatus }) => {
                if (status !== 'READY') return <PendingView />;
                return (
                    <View style={{position: 'absolute', left: this.state.origin.x, top: this.state.origin.y, 
                                  width: this.state.bounds.width, height: this.state.bounds.height, borderWidth: 1, 
                                  borderColor: 'white' }}>

                    </View>
                );
              }}
            </RNCamera>
          :
            <ImageBackground source={{uri: this.state.tampil}}  style={{width: '100%', height: '100%' }}>
                { 
                  this.state.dataPerson == '' ? 
                    null
                  :
                    this.createThreeButtonAlert()
                }

            </ImageBackground>
        }
      
      </View>
    );
  }

  // takePicture = async function(camera) {
  //   const options = { quality: 0.5, base64: true };
  //   const data = await camera.takePictureAsync(options);
  //   //  eslint-disable-next-line
  //   console.log(data.uri);
  // };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'black',
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  capture: {
    flex: 0,
    backgroundColor: '#fff',
    borderRadius: 5,
    padding: 15,
    paddingHorizontal: 20,
    alignSelf: 'center',
    margin: 20,
  },

  //   boxFace:{
  //   position: 'absolute', 
  //   left: this.state.origin.x, 
  //   top: this.state.origin.y, 
  //   width: this.state.bounds.width, 
  //   height: this.state.bounds.height, 
  //   borderWidth: 1,
  //   borderColor: 'white'
  // }


});