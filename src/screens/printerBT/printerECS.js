/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {View, Text, SafeAreaView, TouchableOpacity} from 'react-native';
import {
  BluetoothManager,
  BluetoothEscposPrinter,
  BluetoothTscPrinter,
  DeviceEventEmitter,
} from 'react-native-bluetooth-escpos-printer';
import {logo} from '../../config/imageBas64';

export default class printerECS extends Component {
  constructor(props) {
    super(props);
    this.state = {
      paired: [],
      devices: [],
    };
  }

  componentDidMount = async () => {
    // this.checkBT()
    // DeviceEventEmitter.addListener(
    //     BluetoothManager.EVENT_DEVICE_ALREADY_PAIRED, (rsp)=> {
    //         console.log(rsp) // rsp.devices would returns the paired devices array in JSON string.
    //     });
    // DeviceEventEmitter.addListener(
    //     BluetoothManager.EVENT_DEVICE_FOUND, (rsp)=> {
    //         console.log(rsp)// rsp.devices would returns the found device object in JSON string
    //     });
    this.checkDevices();
    //65351114
  };

  checkDevices() {
    BluetoothManager.scanDevices().then(
      (s) => {
        var ss = JSON.parse(s); //JSON string
        console.log('ss', ss);
        this.setState({
          paired: ss.paired,
          devices: ss.found,
        });
        // if (ss.paired !== []) {
        //     this.pairingDevice(ss.paired[0])
        // }
      },
      (er) => {
        console.log('er', er);
        alert('error' + JSON.stringify(er));
      },
    );
  }

  pairingDevice(rowData) {
    console.log(rowData);
    BluetoothManager.connect(rowData.address) // the device address scanned.
      .then(
        (s) => {
          console.log(s);
        },
        (e) => {
          console.log(e);
          alert(e);
        },
      );
  }

  unPairDevice() {
    BluetoothManager.connect(rowData.address).then(
      (s) => {
        console.log(s);
      },
      (err) => {
        console.log(err);
      },
    );
  }

  disconnectBT() {
    BluetoothManager.disableBluetooth().then(
      () => {
        // do something.
      },
      (err) => {
        alert(err);
      },
    );
  }

  connectBT() {
    BluetoothManager.enableBluetooth().then(
      (r) => {
        var paired = [];
        if (r && r.length > 0) {
          for (var i = 0; i < r.length; i++) {
            try {
              paired.push(JSON.parse(r[i])); // NEED TO PARSE THE DEVICE INFORMATION
            } catch (e) {
              //ignore
            }
          }
        }
        console.log(JSON.stringify(paired));
      },
      (err) => {
        alert(err);
      },
    );
  }

  checkBT() {
    BluetoothManager.isBluetoothEnabled().then(
      (enabled) => {
        alert(enabled); // enabled ==> true /false
      },
      (err) => {
        alert(err);
      },
    );
  }

  async printBT() {
    //with normal text, mine is 58mm mini thermal printer and has 32char limitation
    await BluetoothEscposPrinter.printText(
      '------------------------------\r\n',
      {},
    );
    await BluetoothEscposPrinter.printText('This is another text', {});
    await BluetoothEscposPrinter.printText(
      '------------------------------\r\n',
      {},
    );
    //with columns, split to 4 columns of 8 char each, need to specify alignment for each column and text for each column
    //     await BluetoothEscposPrinter.printColumn([8,8,8,8],[BluetoothEscposPrinter.ALIGN.RIGHT,BluetoothEscposPrinter.ALIGN.RIGHT,BluetoothEscposPrinter.ALIGN.RIGHT,BluetoothEscposPrinter.ALIGN.RIGHT],
    //             ["column1","column2","column3","column4"],{});
    //     await BluetoothEscposPrinter.printColumn([8,8,8,8],[BluetoothEscposPrinter.ALIGN.RIGHT,BluetoothEscposPrinter.ALIGN.RIGHT,BluetoothEscposPrinter.ALIGN.RIGHT,BluetoothEscposPrinter.ALIGN.RIGHT],
    //             ["1","PRODUCTA","2","$2.00"],{});
    //     await BluetoothEscposPrinter.printColumn([8,8,8,8],[BluetoothEscposPrinter.ALIGN.RIGHT,BluetoothEscposPrinter.ALIGN.RIGHT,BluetoothEscposPrinter.ALIGN.RIGHT,BluetoothEscposPrinter.ALIGN.RIGHT],
    //             ["2","PRODUCTB","3","$1.00"],{});
    // //    await BluetoothEscposPrinter.selfTest((res)=> {
    // //         console.log(res)
    //     });
  }

  async printLabel() {
    await BluetoothTscPrinter.printLabel({
      width: 40,
      height: 30,
      gap: 20,
      direction: BluetoothTscPrinter.DIRECTION.FORWARD,
      reference: [0, 0],
      tear: BluetoothTscPrinter.TEAR.ON,
      sound: 1,
      text: [
        {
          text: 'I am a testing txt',
          x: 20,
          y: 0,
          fonttype: BluetoothTscPrinter.FONTTYPE.SIMPLIFIED_CHINESE,
          rotation: BluetoothTscPrinter.ROTATION.ROTATION_0,
          xscal: BluetoothTscPrinter.FONTMUL.MUL_1,
          yscal: BluetoothTscPrinter.FONTMUL.MUL_1,
        },
        {
          text: 'Nasi Uwu?',
          x: 20,
          y: 50,
          fonttype: BluetoothTscPrinter.FONTTYPE.SIMPLIFIED_CHINESE,
          rotation: BluetoothTscPrinter.ROTATION.ROTATION_0,
          xscal: BluetoothTscPrinter.FONTMUL.MUL_1,
          yscal: BluetoothTscPrinter.FONTMUL.MUL_1,
        },
      ],
      qrcode: [
        {
          x: 20,
          y: 96,
          level: BluetoothTscPrinter.EEC.LEVEL_L,
          width: 3,
          rotation: BluetoothTscPrinter.ROTATION.ROTATION_0,
          code: 'show me the money',
        },
      ],
      barcode: [
        {
          x: 120,
          y: 96,
          type: BluetoothTscPrinter.BARCODETYPE.CODE128,
          height: 40,
          readable: 1,
          rotation: BluetoothTscPrinter.ROTATION.ROTATION_0,
          code: '1234567890',
        },
      ],
      image: [
        {
          x: 160,
          y: 160,
          mode: BluetoothTscPrinter.BITMAP_MODE.OVERWRITE,
          width: 80,
          image: logo,
        },
      ],
    }).then(
      () => {
        alert('done');
      },
      (err) => {
        alert(err);
      },
    );
  }

  async printQrcode() {
    await BluetoothEscposPrinter.printQRCode(
      'makanan',
      180,
      BluetoothEscposPrinter.ERROR_CORRECTION.L,
    ); //.then(()=>{alert('done')},(err)=>{alert(err)});
    // await  BluetoothEscposPrinter.printText("\r\n\r\n\r\n", {});
  }

  async printBarcode() {
    await BluetoothEscposPrinter.printBarCode(
      '123456789012',
      BluetoothEscposPrinter.BARCODETYPE.CODE128,
      3,
      100,
      0,
      2,
    );
    // await  BluetoothEscposPrinter.printText("\r\n\r\n\r\n", {});
  }

  async printImage() {
    await BluetoothEscposPrinter.printPic(logo, {width: 200, left: 40});
    // await  BluetoothEscposPrinter.printText("\r\n\r\n\r\n", {});
  }

  render() {
    return (
      <SafeAreaView
        style={{
          width: '100%',
          height: '100%',
          backgroundColor: 'black',
          padding: 15,
        }}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            marginBottom: 15,
          }}>
          <View>
            <Text style={{color: 'white', fontSize: 17, fontWeight: 'bold'}}>
              Printer Bluetooth ECS/POS
            </Text>
            <Text style={{fontSize: 15, color: 'white'}}>65351114</Text>
          </View>
          <TouchableOpacity
            style={{
              width: 60,
              height: 30,
              backgroundColor: 'lightgrey',
              borderRadius: 5,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text style={{fontSize: 13, fontWeight: 'bold'}}>Refresh</Text>
          </TouchableOpacity>
        </View>

        {this.state.devices !== []
          ? this.state.devices.map((device) => {
              return (
                <View
                  key={device.address}
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    width: '100%',
                    backgroundColor: 'lightgrey',
                    borderRadius: 5,
                    marginBottom: 5,
                    padding: 7,
                    alignItems: 'center',
                  }}>
                  <View>
                    <Text style={{fontWeight: 'bold', fontSize: 14}}>
                      {device.name}
                    </Text>
                    <Text style={{color: 'grey', fontSize: 12}}>
                      {device.address}
                    </Text>
                  </View>

                  <TouchableOpacity
                    style={{
                      paddingLeft: 10,
                      paddingRight: 10,
                      paddingTop: 3,
                      paddingBottom: 3,
                      backgroundColor: 'maroon',
                      borderRadius: 5,
                    }}
                    onPress={() => this.disconnect()}>
                    <Text style={{fontSize: 14, color: 'white'}}>
                      Disconnect
                    </Text>
                  </TouchableOpacity>

                  <TouchableOpacity
                    style={{
                      paddingLeft: 10,
                      paddingRight: 10,
                      paddingTop: 3,
                      paddingBottom: 3,
                      backgroundColor: 'navy',
                      borderRadius: 5,
                    }}
                    onPress={() => this.pairingDevice(device)}>
                    <Text style={{fontSize: 14, color: 'white'}}>Pair</Text>
                  </TouchableOpacity>
                </View>
              );
            })
          : null}

        {this.state.paired !== []
          ? this.state.paired.map((device) => {
              return (
                <View
                  key={device.address}
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    width: '100%',
                    backgroundColor: 'lightgrey',
                    borderRadius: 5,
                    marginBottom: 5,
                    padding: 7,
                    alignItems: 'center',
                  }}>
                  <View>
                    <Text style={{fontWeight: 'bold', fontSize: 14}}>
                      {device.name}
                    </Text>
                    <Text style={{color: 'grey', fontSize: 12}}>
                      {device.address}
                    </Text>
                  </View>

                  <TouchableOpacity
                    style={{
                      paddingLeft: 10,
                      paddingRight: 10,
                      paddingTop: 3,
                      paddingBottom: 3,
                      backgroundColor: 'maroon',
                      borderRadius: 5,
                    }}
                    onPress={() => this.disconnect()}>
                    <Text style={{fontSize: 14, color: 'white'}}>
                      Disconnect
                    </Text>
                  </TouchableOpacity>

                  <TouchableOpacity
                    style={{
                      paddingLeft: 10,
                      paddingRight: 10,
                      paddingTop: 3,
                      paddingBottom: 3,
                      backgroundColor: 'navy',
                      borderRadius: 5,
                    }}
                    onPress={() => this.pairingDevice(device)}>
                    <Text style={{fontSize: 14, color: 'white'}}>Pair</Text>
                  </TouchableOpacity>
                </View>
              );
            })
          : null}

        <View style={{marginTop: 10}}>
          <TouchableOpacity
            style={{
              width: '100%',
              height: 45,
              backgroundColor: 'white',
              borderRadius: 5,
              justifyContent: 'center',
              alignItems: 'center',
              marginBottom: 10,
            }}
            onPress={() => this.printBT()}>
            <Text style={{fontWeight: 'bold', fontSize: 15}}>Demo Print</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              width: '100%',
              height: 45,
              backgroundColor: 'white',
              borderRadius: 5,
              justifyContent: 'center',
              alignItems: 'center',
              marginBottom: 10,
            }}
            onPress={() => this.printLabel()}>
            <Text style={{fontWeight: 'bold', fontSize: 15}}>Demo Label</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              width: '100%',
              height: 45,
              backgroundColor: 'white',
              borderRadius: 5,
              justifyContent: 'center',
              alignItems: 'center',
              marginBottom: 10,
            }}
            onPress={() => this.printQrcode()}>
            <Text style={{fontWeight: 'bold', fontSize: 15}}>Demo QRCODE</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              width: '100%',
              height: 45,
              backgroundColor: 'white',
              borderRadius: 5,
              justifyContent: 'center',
              alignItems: 'center',
              marginBottom: 10,
            }}
            onPress={() => this.printBarcode()}>
            <Text style={{fontWeight: 'bold', fontSize: 15}}>Demo BARCODE</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              width: '100%',
              height: 45,
              backgroundColor: 'white',
              borderRadius: 5,
              justifyContent: 'center',
              alignItems: 'center',
              marginBottom: 10,
            }}
            onPress={() => this.printImage()}>
            <Text style={{fontWeight: 'bold', fontSize: 15}}>Demo IMAGE</Text>
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    );
  }
}
