/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {View, Text, SafeAreaView, TouchableOpacity} from 'react-native';
import BluetoothSerial from 'react-native-bluetooth-serial';
import {Buffer} from 'buffer';
global.Buffer = Buffer;
const iconv = require('iconv-lite');

export default class checkDevices extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listDevices: [],
      status: false,
      pair: false,
    };
  }

  componentDidMount = () => {
    this.refresh();
    this.isConnected();
    BluetoothSerial.on('error', (err) => console.log(`Error: ${err.message}`));
    BluetoothSerial.on('connectionLost', (res) => console.log('lost ', res));
  };

  allertBluetooth() {
    alert('Bluetooth Not Enable !!');
  }

  requestEnable() {
    BluetoothSerial.requestEnable()
      .then((res) => console.log(res))
      .catch((err) => console.log(err.message));
  }

  refresh() {
    Promise.all([BluetoothSerial.isEnabled(), BluetoothSerial.list()]).then(
      (values) => {
        const [isEnabled, devices] = values;
        console.log(isEnabled, devices);
        isEnabled == false ? this.allertBluetooth() : null;
        this.setState({
          listDevices: devices,
          status: isEnabled,
        });
      },
    );
  }

  connect(device) {
    // this.setState({ connecting: true })
    BluetoothSerial.connect(device.id)
      .then((res) => {
        this.setState({
          pair: true,
        });
        console.log(res);
        console.log(device);
      })
      .catch((err) => console.log('error', err.message));
  }

  disconnect() {
    BluetoothSerial.disconnect()
      .then(() => this.setState({pair: false}))
      .catch((err) => console.log(err.message));
  }

  isConnected() {
    BluetoothSerial.isConnected()
      .then((res) => {
        console.log('isConnect Devices ', res);
        this.setState({
          pair: true,
        });
      })
      .catch((err) => console.log(err.message));
  }

  // function write single text
  write(message) {
    console.log('massage', message);
    BluetoothSerial.write(message)
      .then((res) => console.log('print ', res))
      .catch((err) => console.log(err.message));
  }

  writePackets(message, packetSize = 64) {
    console.log(packetSize);
    const toWrite = iconv.encode(message, 'cp852');
    const writePromises = [];
    const packetCount = Math.ceil(toWrite.length / packetSize);

    for (var i = 0; i < packetCount; i++) {
      const packet = new Buffer(packetSize);
      packet.fill(' ');
      toWrite.copy(packet, 0, i * packetSize, (i + 1) * packetSize);
      writePromises.push(BluetoothSerial.write(packet));
    }

    Promise.all(writePromises).then((result) => {
      console.log('res write', result);
    });
  }

  textL1() {
    return <Text>adwda</Text>;
  }

  render() {
    return (
      <SafeAreaView
        style={{
          width: '100%',
          height: '100%',
          backgroundColor: 'black',
          padding: 15,
        }}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            marginBottom: 15,
          }}>
          <Text style={{color: 'white', fontSize: 17, fontWeight: 'bold'}}>
            Printer Bluetooth P25
          </Text>
          <TouchableOpacity
            style={{
              width: 60,
              height: 30,
              backgroundColor: 'lightgrey',
              borderRadius: 5,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text style={{fontSize: 13, fontWeight: 'bold'}}>Refresh</Text>
          </TouchableOpacity>
        </View>

        {this.state.listDevices !== []
          ? this.state.listDevices.map((device) => {
              return (
                <View
                  key={device.class}
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    width: '100%',
                    backgroundColor: 'lightgrey',
                    borderRadius: 5,
                    marginBottom: 5,
                    padding: 7,
                    alignItems: 'center',
                  }}>
                  <View>
                    <Text style={{fontWeight: 'bold', fontSize: 14}}>
                      {device.name}
                    </Text>
                    <Text style={{color: 'grey', fontSize: 12}}>
                      {device.address}
                    </Text>
                  </View>
                  {this.state.pair == false ? (
                    <TouchableOpacity
                      style={{
                        paddingLeft: 10,
                        paddingRight: 10,
                        paddingTop: 3,
                        paddingBottom: 3,
                        backgroundColor: 'navy',
                        borderRadius: 5,
                      }}
                      onPress={() => this.connect(device)}>
                      <Text style={{fontSize: 14, color: 'white'}}>Pair</Text>
                    </TouchableOpacity>
                  ) : (
                    <TouchableOpacity
                      style={{
                        paddingLeft: 10,
                        paddingRight: 10,
                        paddingTop: 3,
                        paddingBottom: 3,
                        backgroundColor: 'maroon',
                        borderRadius: 5,
                      }}
                      onPress={() => this.disconnect()}>
                      <Text style={{fontSize: 14, color: 'white'}}>
                        Disconnect
                      </Text>
                    </TouchableOpacity>
                  )}
                </View>
              );
            })
          : null}

        <View style={{marginTop: 10}}>
          <TouchableOpacity
            style={{
              width: '100%',
              height: 45,
              backgroundColor: 'white',
              borderRadius: 5,
              justifyContent: 'center',
              alignItems: 'center',
            }}
            onPress={() => this.writePackets(this.textL1())}>
            <Text style={{fontWeight: 'bold', fontSize: 15}}>Demo Print</Text>
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    );
  }
}

const text_2 = 'ini makan';
const text_1 = `
Lorem Ipsum is simply du
mmy text of the printing
and typesetting industry.
Lorem Ipsum has been the
industry standard dummy 
text ever since the 1500
s, when an unknown printer
took a galley of type 
and scrambled it to make 
a type specimen book.
It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.

${text_2}


`;
