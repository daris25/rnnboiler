import React from "react";
import { AppRegistry, Image, StatusBar } from "react-native";
import { Container, Content, Text, List, ListItem } from "native-base";


const routes = [
  {text: "Home", form: 'fooTabs'}, 
  {text: "Face Detect", form: 'detectFace'}, 
];

export default class SideBar extends React.Component {
  render() {
    return (
      <Container>
        <Content>
          <Image 
            style={{width: '100%', height: 200}}
            source={{uri : 'https://i.dlpng.com/static/png/6368440_preview.png'}}
          />
          <List
            dataArray={routes}
            renderRow={data => {
              return (
                <ListItem
                  button
                  onPress={() => console.log(data.form)}>
                  <Text>{data.text}</Text>
                </ListItem>
              );
            }}
          />
        </Content>
      </Container>
    );
  }
}