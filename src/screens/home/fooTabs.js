import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { Tab, Tabs, TabHeading, Icon } from 'native-base';

import One from './sc_Footabs/screenOne';

export default class fooTabs extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View style={{flex: 1, width: '100%', height: '100%'}}>
      
        <Tabs tabBarPosition="bottom">
          <Tab heading={ 
                <TabHeading>
                    <Icon name="camera" />
                </TabHeading>}>
            <One {...this.props}/>
          </Tab>

          <Tab heading={ 
                <TabHeading>
                    <Icon name="apps" />
                </TabHeading>}>
            <One {...this.props}/>
          </Tab>
        </Tabs>
      </View>
    );
  }
}
