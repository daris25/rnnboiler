/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {Drawer} from 'native-base';

import SideBar from './sideBar';
import {pushScreen} from '../../route/control';

export default class index extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  closeDrawer = () => {
    this.drawer._root.close();
  };

  openDrawer = () => {
    this.drawer._root.open();
  };

  render() {
    return (
      <Drawer
        ref={(ref) => {
          this.drawer = ref;
        }}
        content={<SideBar navigator={this.navigator} />}
        onClose={() => this.closeDrawer()}>
        <View style={{backgroundColor: 'black', width: '100%', height: '100%'}}>
          <View
            style={{
              height: 50,
              justifyContent: 'flex-end',
              flexDirection: 'row',
              alignItems: 'center',
              padding: 15,
            }}>
            <TouchableOpacity onPress={() => this.openDrawer()}>
              <Text style={{fontSize: 15, color: 'white', fontWeight: 'bold'}}>
                More
              </Text>
            </TouchableOpacity>
          </View>

          <View
            style={{
              backgroundColor: 'grey',
              width: '90%',
              height: 80,
              alignSelf: 'center',
              borderRadius: 5,
              opacity: 0.7,
              justifyContent: 'center',
              padding: 15,
            }}>
            <Text style={{fontSize: 20, fontWeight: 'bold', color: 'white'}}>
              React Native Boiler Plate
            </Text>
            <Text style={{fontSize: 13, color: 'white'}}>
              Instagram : dariss25
            </Text>
          </View>

          <View
            style={{
              marginTop: 5,
              flexDirection: 'row',
              flexWrap: 'wrap',
              padding: 10,
              paddingLeft: 20,
              paddingRight: 20,
              justifyContent: 'space-between',
            }}>
            <TouchableOpacity
              onPress={() => pushScreen(this.props.componentId, 'detectFace')}
              style={{
                width: '49%',
                height: 160,
                backgroundColor: 'grey',
                elevation: 10,
                borderRadius: 5,
                opacity: 0.7,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text style={{fontSize: 20, fontWeight: 'bold', color: 'white'}}>
                Face Detect
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => pushScreen(this.props.componentId, 'inputCode')}
              style={{
                width: '49%',
                height: 160,
                backgroundColor: 'grey',
                elevation: 10,
                borderRadius: 5,
                opacity: 0.7,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text style={{fontSize: 20, fontWeight: 'bold', color: 'white'}}>
                Phone Auth
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => pushScreen(this.props.componentId, 'checkDevices')}
              style={{
                width: '49%',
                height: 160,
                backgroundColor: 'grey',
                elevation: 10,
                marginTop: 7,
                padding: 10,
                borderRadius: 5,
                opacity: 0.7,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  fontSize: 20,
                  fontWeight: 'bold',
                  color: 'white',
                  textAlign: 'center',
                }}>
                Printer Bluethoot
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => pushScreen(this.props.componentId, 'printerECS')}
              style={{
                width: '49%',
                height: 160,
                backgroundColor: 'grey',
                elevation: 10,
                marginTop: 7,
                padding: 10,
                borderRadius: 5,
                opacity: 0.7,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  fontSize: 20,
                  fontWeight: 'bold',
                  color: 'white',
                  textAlign: 'center',
                }}>
                Printer ECS/POS
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Drawer>
    );
  }
}
