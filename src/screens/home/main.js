import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';

import sideBar from './index';

export default class main extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
        <View style={{height: 50, backgroundColor: 'grey', justifyContent: 'flex-end', flexDirection: 'row', alignItems: 'center', padding: 15}}> 
                  <TouchableOpacity onPress={()=> sideBar.abc()}>
                      <Text style={{fontSize: 15, color: 'white', fontWeight: 'bold'}}>More</Text>
                  </TouchableOpacity>
        </View>
    );
  }
}
