import React, { Component, useState, useEffect } from 'react';
import { View, Text, TextInput, TouchableOpacity } from 'react-native';
import auth from '@react-native-firebase/auth';

function Terms(props){
    // console.log('asu',props)

    const [code, setCode] = useState('123456')
    const [phone, setPhone] = useState('+6281317383720')
    const [confirm, setConfirm] = useState(null);

        console.log(code)

    useEffect(() => {
      async function likeCDM(params) {
            console.log('asu')

            _sentSMS()
        }
        likeCDM();
    }, []);

    const _sentSMS = async ()=>{
      console.log( 'asu', phone)
        // const confirmation = await auth().signInWithPhoneNumber(phone,true);
        //   setConfirm(confirmation);
        //   console.log('sent OTP', confirmation)

        auth().signInWithPhoneNumber(phone)
        .then(confirmResult => console.log('asu', confirmResult))
        .catch(error => console.log('asu',error));
          
    }

    return (
      <View style={{backgroundColor: 'black', height: '100%', justifyContent: 'center', alignItems: 'center', padding: 50}}>
            <TextInput 
                style={{backgroundColor: 'white', height: 45, width: '100%', textAlign: 'center', fontSize: 20, fontWeight: 'bold'}}
                keyboardType='number-pad'
                value={code}
                onChangeText={(txt)=> setCode(txt)}
            />
            <TouchableOpacity style={{alignSelf: 'flex-end', marginTop: 15}}>
                <Text style={{color: 'white', marginBottom: 15}}>Re-sent Code</Text>
            </TouchableOpacity>

            <TouchableOpacity style={{borderWidth: 1, borderColor: 'white', height: 50, width: '100%', borderRadius: 30, justifyContent: 'center'}}>
                <Text style={{textAlign: 'center', fontSize: 14, color: 'white'}}>Submit</Text>
            </TouchableOpacity>
      </View>
    );
  }

export default Terms;
