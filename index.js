// /**
//  * @format
//  */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => App);


// import { Navigation } from "react-native-navigation";
// import App from "./App";
// Navigation.registerComponent('com.myApp.WelcomeScreen', () => App);

// Navigation.events().registerAppLaunchedListener(() => {
//    Navigation.setRoot({
//     root: {
//         stack: {
//           id: 'Intro',
//           children: [
//             {
//               component: {
//                 name: 'com.myApp.WelcomeScreen',
//                 options: {
//                   topBar: {
//                     visible: false,
//                     height: 0
//                   }
//                 }
//               }
//             }
//         ],
//         options: {
//             statusBar: {
//               backgroundColor: 'white',
//               visible: true
//             },
//           }
//         }
//       }
//   });
// });