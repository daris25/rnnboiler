import {Navigation} from 'react-native-navigation';
import {registerScreens} from './src/route'
    registerScreens();
import {setRoot} from './src/route/control'

console.disableYellowBox = true
console.ignoredYellowBox = ['Warning: Each', 'Warning: Failed'];
console.log('app')

Navigation.events().registerAppLaunchedListener(() => {
  setRoot('Home')
});